# -*- coding: utf-8 -*-

import os.path
import sys

sys.path.append(os.path.pardir)

import setuputils

extensions = []

templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

project = u'ged'
copyright = u'2013, Berker Peksag'

version = release = setuputils.find_version('ged.py')

exclude_patterns = ['_build']

pygments_style = 'sphinx'

html_theme = 'nature'

html_static_path = ['_static']

htmlhelp_basename = 'geddoc'
