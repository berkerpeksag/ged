ged: A simple script runner
===========================

:Source: https://github.com/berkerpeksag/ged/
:Issues: https://github.com/berkerpeksag/ged/issues/
:PyPI: http://pypi.python.org/pypi/ged/


Getting Started
---------------

Install with **pip**:

.. code-block:: bash

    $ [sudo] pip install ged

or clone the latest version from GitHub_.


Usage
-----

By default, ged will try to find a ``.gedrc`` or ``gedrc`` file in the
``$HOME`` directory.

Basic ``.gedrc`` configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: ini

    [settings]
    scripts_dir = ~/scripts

.. versionchanged:: 0.2
   The *core* section name was renamed to *settings*.

Making your first script for ged
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ged will look for a ``main()`` function as an entry point in the script
file, so all scripts should have a ``main()`` function. Let's create
a file named ``hello.py`` in the ``~/scripts`` directory::

    """Our first Ged script."""

    def hello(name):
        print 'Hello %s!' % name


    def main(argv):
        name = argv[0]
        hello(name)

Then run it:

.. code-block:: bash

    $ ged hello berker
    Hello berker!

Defining aliases
^^^^^^^^^^^^^^^^

.. versionadded:: 0.2

Put the following snippet to your ``.gedrc`` file:

.. code-block:: ini

    [aliases]
    hola = hello berker
    hei = hello

Then run it:

.. code-block:: bash

    $ ged hola
    Hello berker!
    $ ged hei lindsay
    Hello lindsay!


License
-------

All files that are part of this project are covered by the following
license, except where explicitly noted.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.


.. _GitHub: https://github.com/berkerpeksag/ged/
