ged: A simple script runner
---------------------------


Project details
===============

Code:
    http://github.com/berkerpeksag/ged

Documentation:
    http://ged.rtfd.org/

PyPI:
    http://pypi.python.org/pypi/ged/

License:
    Mozilla Public License v.2.0
